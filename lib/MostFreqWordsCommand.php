<?php

namespace Lib;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MostFreqWordsCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('count:most-freq-words')
            ->setDescription('Count the most frequent words in the specified file.')
            ->setHelp('This command allows you to count the most frequent words in the specified file.');

        $this
            ->addArgument('file', InputArgument::REQUIRED, 'The file with words to count')
            ->addArgument('limit', InputArgument::REQUIRED, 'Limit the number of outputted words');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputFilePath = $input->getArgument('file');
        $limit = (int)$input->getArgument('limit');

        if (!file_exists($inputFilePath)) {
            $output->writeln("<error>Invalid file path: [$inputFilePath]</error>");
            return 1;
        }

        $words = file($inputFilePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if ($words === false) {
            $output->writeln("<error>Cannot read file: [$inputFilePath]</error>");
            return 1;
        }

        $freqWords = (new MostFreqWords($words))->count();
        $limitedFreqWords = (new Limited($freqWords, $limit))->values();

        $tableRows = [];
        foreach ($limitedFreqWords as $word => $freq) {
            $tableRows[] = [$word, $freq];
        }

        $table = new Table($output);
        $table
            ->setHeaders(['Word', 'Frequency'])
            ->setRows($tableRows)
            ->render();

        return 0;
    }
}