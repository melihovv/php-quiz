<?php

namespace Lib;

class Limited
{
    /**
     * @var array
     */
    private $values;

    /**
     * @var int
     */
    private $limit;

    public function __construct(array $values, $limit)
    {
        $this->values = $values;
        $this->limit = $limit;
    }

    public function values()
    {
        if ($this->limit < 1) {
            return $this->values;
        }

        return array_slice($this->values, 0, $this->limit);
    }
}