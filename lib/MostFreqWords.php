<?php

namespace Lib;

class MostFreqWords
{
    /**
     * @var array
     */
    private $words;

    public function __construct(array $words)
    {
        $this->words = $words;
    }

    public function count()
    {
        $freqWords = array_count_values($this->words);
        arsort($freqWords);

        return $freqWords;
    }
}