<?php

require_once __DIR__ . '/vendor/autoload.php';

use Lib\MostFreqWordsCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new MostFreqWordsCommand());

$application->run();
