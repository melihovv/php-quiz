<?php

use Lib\MostFreqWords;

class MostFreqWordsTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function it_counts_most_frequent_words()
    {
        $words = ['a', 'b', 'c', 'a', 'c', 'c'];
        $freqWords = (new MostFreqWords($words))->count();

        $this->assertEquals([
            'a' => 2,
            'b' => 1,
            'c' => 3,
        ], $freqWords);
    }
}
