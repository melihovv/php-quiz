<?php

use Lib\Limited;

class LimitedTest extends \PHPUnit\Framework\TestCase
{
    /** @test */
    public function it_limit_passed_array_to_specified_limit_number()
    {
        $values = [1, 2, 3, 4];
        $limitedValues = (new Limited($values, 2))->values();

        $this->assertEquals([1, 2], $limitedValues);
    }

    /** @test */
    public function it_returns_original_array_if_limit_below_one()
    {
        $values = [1, 2, 3, 4];
        $limitedValues = (new Limited($values, 0))->values();

        $this->assertEquals([1, 2, 3, 4], $limitedValues);
    }
}
